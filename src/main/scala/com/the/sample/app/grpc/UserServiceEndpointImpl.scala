package com.the.sample.app.grpc

import com.the.sample.app.model.User
import com.google.protobuf.empty.Empty
import com.the.sample.app.grpc.UserServiceEndpoint.{CreateUserRequest, UpdateUserRequest, UserByEmailRequest, UserByIdRequest, UserDto, UserResponse, UserServiceEndpointGrpc}
import com.the.sample.app.service.UserService
import org.lognet.springboot.grpc.GRpcService

import scala.concurrent.Future
@GRpcService
class UserServiceEndpointImpl(val userService: UserService) extends UserServiceEndpointGrpc.UserServiceEndpoint{
  override def findById(request: UserByIdRequest): Future[UserResponse] =
    Future.successful(UserResponse(user = Option(request).map(_.id).
      flatMap(userService.findById(_)).map(user=>
      UserDto(id = user.id,
        email = user.email,
        fullName = user.fullName))))


  override def findByEmail(request: UserByEmailRequest): Future[UserResponse] =
    Future.successful(UserResponse(user = Option(request).map(_.email).
      flatMap(userService.findByEmail(_)).map(user=>
      UserDto(id = user.id,
        email = user.email,
        fullName = user.fullName))))

  override def save(request: CreateUserRequest): Future[Empty] = {
    Option(request).
      map(req=>new User(req.email,req.fullName)).
        foreach(userService.save(_))
    Future.successful(Empty.defaultInstance)
  }

  override def update(request: UpdateUserRequest): Future[Empty] = {
    Option(request).
      map(req=>{
        val user = new User(req.email,req.fullName)
        user.id = req.id
        user
      }).
      foreach(userService.save(_))
    Future.successful(Empty.defaultInstance)
  }

  override def deleteById(request: UserByIdRequest): Future[Empty] = {
    Option(request).
      map(_.id).
      foreach(userService.deleteById(_))
    Future.successful(Empty.defaultInstance)
  }
}
