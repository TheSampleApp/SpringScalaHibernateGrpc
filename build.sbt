ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter-data-jpa" % "3.0.4"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter-web" % "3.0.4"
//We have added H2, however you can mysql, Postgres
libraryDependencies += "com.h2database" % "h2" % "2.1.214"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter-test" % "3.0.4" % Test
lazy val root = (project in file("."))
  .settings(
    name := "SpringScalaHibernateGrpc"
  )
libraryDependencies ++= Seq(
  "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
  "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion
)

Compile / PB.targets := Seq(
  scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
)

val grpcJavaVersion = "1.52.1"
libraryDependencies ++= Seq(
  "io.grpc"          % "grpc-all"             % grpcJavaVersion,
  "io.grpc" % "grpc-protobuf" % grpcJavaVersion,
  "io.grpc" % "grpc-stub" % grpcJavaVersion,
  "io.grpc" % "grpc-netty" % grpcJavaVersion,
  "io.github.lognet" % "grpc-spring-boot-starter" % "5.0.0" exclude("io.grpc","grpc-netty-shaded"),
  "javax.annotation" % "javax.annotation-api" % "1.3.2" // needed for grpc-java on JDK9
)

libraryDependencies ++= Seq(
  "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf"
)